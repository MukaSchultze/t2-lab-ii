#include <stdlib.h>
#include <stdio.h>
#include "professor.h"
#include "io.h"
#include "list.h"

void CadastrarProfessor(Cadastros* c) {
	Professor* p = malloc(sizeof(Professor));

	PrintLine("Informacoes do Professor");

	p->nome = ReadString("Nome: ");
	p->area = ReadString("Area de atuacao: ");
	p->titulacao = ReadString("Titulacao: ");
	p->siape = 0;

	while (p->siape == 0)
		p->siape = ReadInt("SIAPE: ");

	AddValueToList(c->professoresCadastrados, p);
}

void RemoverProfessor(Cadastros* c) {
	PrintLine("Remover Professor\n");
	ListarProfessores(c->professoresCadastrados);
	int siape = ReadInt("\nSIAPE (0 para voltar): ");

	if (siape == 0)
		return;

	for (ListNode* node = c->professoresCadastrados->firstElement; node != NULL; node = node->next) {
		Professor* p = node->value;

		if (p->siape == siape) {
			printf("%s (%d) removido\n", p->nome, p->siape);
			free(p->nome);
			free(p->area);
			free(p->titulacao);
			RemoveNodeFromList(c->professoresCadastrados, node);
			return;
		}
	}

	PrintLine("SIAPE nao encontrado");
}

void BuscarProfessor(Cadastros* c) {
	PrintLine("Buscar Professor\n");
	ListarProfessores(c->professoresCadastrados);
	int siape = ReadInt("\nSIAPE (0 para voltar): ");
	PrintLine("");

	if (siape == 0)
		return;

	Professor* p = BuscarProfessorSiape(c->professoresCadastrados, siape);

	if (p != NULL)
		PrintProfessor(*p);
	else
		PrintLine("Professor nao encontrado");
}

void PrintProfessor(Professor p) {
	printf("Nome: %s\nSIAPE: %d\nTitulacao: %s\nArea de Atuacao: %s\n", p.nome, p.siape, p.titulacao, p.area);
}

Professor* BuscarProfessorSiape(List* l, int siape) {
	for (ListNode* node = l->firstElement; node != NULL; node = node->next) {
		Professor* p = node->value;

		if (p->siape == siape)
			return p;
	}

	return NULL;
}