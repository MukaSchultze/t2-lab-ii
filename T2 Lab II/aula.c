#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>
#include "aula.h"
#include "turma.h"
#include "aluno.h"
#include "io.h"

void LancarAula(Cadastros* c) {
	ListarTurmas(c->turmasCadastradas);
	Turma* t = BuscarTurmaCodigo(c->turmasCadastradas, ReadInt("Codigo da turma: "));

	if (t == NULL) {
		PrintLine("Turma nao encontrada");
		return;
	}

	ListarDisciplinas(t->disciplinas);
	Disciplina* d = BuscarDisciplinaCodigo(c->disciplinasCadastradas, ReadInt("Codigo da disciplina: "));

	if (d == NULL) {
		PrintLine("Disciplina nao encontrada");
		return;
	}

	Aula* a = malloc(sizeof(Aula));

	printf("Data (dd/mm/aaaa): ");
	scanf("%d/%d/%d", &a->dia, &a->mes, &a->ano);
	a->conteudo = ReadString("Conteudo: ");
	a->codigoDisciplina = d->codigo;
	a->matriculaAlunosPresentes = CreateNewList();

	for (ListNode* n = t->alunos->firstElement; n != NULL; n = n->next) {
		Aluno* aluno = n->value;

		printf("%s (%d) presente? (Y/n): ", aluno->nome, aluno->matricula);
		char ch = getchar();

		if (ch == '\n' || ch == '\0')
			ch = getchar();

		if (ch == 'Y' || ch == 'y')
			AddValueToList(a->matriculaAlunosPresentes, Clone(&aluno->matricula, sizeof(int)));
	}

	AddValueToList(t->aulas, a);
}