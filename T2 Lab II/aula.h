#pragma once
#include "disciplina.h"
#include "cadastros.h"

typedef struct {
	int dia;
	int mes;
	int ano;
	char* conteudo;
	int codigoDisciplina;
	List* matriculaAlunosPresentes;
} Aula;

void LancarAula(Cadastros* c);