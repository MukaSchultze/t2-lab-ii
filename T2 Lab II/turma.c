#include <stdlib.h>
#include <stdio.h>
#include "turma.h"
#include "io.h"
#include "list.h"
#include "professor.h"
#include "aluno.h"
#include "avaliacao.h"
#include "disciplina.h"
#include "aula.h"

void CadastrarTurma(Cadastros* c) {
	Turma* t = malloc(sizeof(Turma));

	PrintLine("Informacoes da Turma");

	t->alunos = CreateNewList();
	t->disciplinas = CreateNewList();
	t->professores = CreateNewList();
	t->avaliacoes = CreateNewList();
	t->aulas = CreateNewList();
	t->codigo = 0;

	while (t->codigo == 0)
		t->codigo = ReadInt("Codigo: ");

	AddValueToList(c->turmasCadastradas, t);
}

void RemoverTurma(Cadastros* c) {
	PrintLine("Remover Turma\n");
	ListarTurmas(c->turmasCadastradas);
	int codigo = ReadInt("\nCodigo (0 para voltar): ");

	if (codigo == 0)
		return;

	for (ListNode* node = c->turmasCadastradas->firstElement; node != NULL; node = node->next) {
		Turma* t = node->value;

		if (t->codigo == codigo) {
			printf("Turma %d removida\n", t->codigo);

			for (ListNode* n = t->alunos->firstElement; n != NULL; n = n->next)
				free((*(Aluno*)n->value).nome);

			for (ListNode* n = t->disciplinas->firstElement; n != NULL; n = n->next)
				free((*(Disciplina*)n->value).nome);

			for (ListNode* n = t->professores->firstElement; n != NULL; n = n->next) {
				free((*(Professor*)n->value).nome);
				free((*(Professor*)n->value).titulacao);
				free((*(Professor*)n->value).area);
			}

			for (ListNode* n = t->avaliacoes->firstElement; n != NULL; n = n->next)
				FreeList((*(Avaliacao*)n->value).notas);

			for (ListNode* n = t->aulas->firstElement; n != NULL; n = n->next) {
				free((*(Aula*)n->value).conteudo);
				FreeList((*(Aula*)n->value).matriculaAlunosPresentes);
			}

			FreeList(t->alunos);
			FreeList(t->disciplinas);
			FreeList(t->professores);
			FreeList(t->avaliacoes);
			FreeList(t->aulas);
			RemoveNodeFromList(c->turmasCadastradas, node);
			return;
		}
	}

	PrintLine("Codigo nao encontrado");
}

void BuscarTurma(Cadastros* c) {
	PrintLine("Remover Turma\n");
	ListarTurmas(c->turmasCadastradas);
	int codigo = ReadInt("\nCodigo (0 para voltar): ");
	PrintLine("");

	if (codigo == 0)
		return;

	Turma* t = BuscarTurmaCodigo(c->turmasCadastradas, codigo);

	if (t != NULL)
		PrintTurma(*t);
	else
		PrintLine("Codigo nao encontrado");
}

void PrintTurma(Turma t) {
	printf("Codigo: %d\n", t.codigo);

	PrintLine("Professores");
	for (ListNode* node = t.professores->firstElement; node != NULL; node = node->next) {
		Professor* p = node->value;
		printf("\t%s (%d)\n", p->nome, p->siape);
	}

	PrintLine("Alunos");
	for (ListNode* node = t.alunos->firstElement; node != NULL; node = node->next) {
		Aluno* a = node->value;
		printf("\t%s (%d)\n", a->nome, a->matricula);
	}

	PrintLine("Disciplinas");
	for (ListNode* node = t.disciplinas->firstElement; node != NULL; node = node->next) {
		Disciplina* d = node->value;
		printf("\t%s (%d)\n", d->nome, d->codigo);
	}

}

Turma* BuscarTurmaCodigo(List* l, int codigoTurma) {
	for (ListNode* node = l->firstElement; node != NULL; node = node->next) {
		Turma* t = node->value;

		if (t->codigo == codigoTurma)
			return t;
	}

	return NULL;
}

void MatricularAlunoTurma(Cadastros* c, Turma* t) {
	if (t->alunos->length >= 50) {
		PrintLine("A turma j� possui 50 alunoss");
		return;
	}

	ListarAlunos(c->alunosCadastrados);
	int matricula = ReadInt("\nMatricula do aluno: ");
	Aluno* a = BuscarAlunoMatricula(c->alunosCadastrados, matricula);

	if (BuscarAlunoMatricula(t->alunos, matricula) != NULL) {
		PrintLine("Aluno ja matriculado nessa turma");
		return;
	}

	if (a == NULL) {
		PrintLine("Aluno nao encontrado");
		return;
	}

	a = Clone(a, sizeof(Aluno));
	a->nome = CloneString(a->nome);

	AddValueToList(t->alunos, a);
}

void RemoverAlunoTurma(Cadastros* c, Turma* t) {
	ListarAlunos(c->alunosCadastrados);
	int matricula = ReadInt("\nMatricula do aluno: ");
	Aluno* a = BuscarAlunoMatricula(t->alunos, matricula);

	if (a == NULL) {
		PrintLine("Aluno nao matriculado na turma");
		return;
	}

	free(a->nome);
	RemoveValueFromList(t->alunos, a);
	PrintLine("Aluno removido");
}

void AdicionarProfessorTurma(Cadastros* c, Turma* t) {
	ListarAlunos(c->professoresCadastrados);
	int siape = ReadInt("\nSiape do professor: ");
	Professor* p = BuscarProfessorSiape(c->professoresCadastrados, siape);

	if (BuscarProfessorSiape(t->professores, siape) != NULL) {
		PrintLine("Professor ja registrado nessa turma");
		return;
	}

	if (p == NULL) {
		PrintLine("Professor nao encontrado");
		return;
	}

	p = Clone(p, sizeof(Professor));
	p->nome = CloneString(p->nome);
	p->area = CloneString(p->area);
	p->titulacao = CloneString(p->titulacao);

	AddValueToList(t->professores, p);
}

void RemoverProfessorTurma(Cadastros* c, Turma* t) {
	ListarAlunos(c->professoresCadastrados);
	int siape = ReadInt("\nSiape do professor: ");
	Professor* professor = BuscarProfessorSiape(t->professores, siape);

	if (professor == NULL) {
		PrintLine("Professor nao cadastrado na turma");
		return;
	}

	free(professor->nome);
	free(professor->area);
	free(professor->titulacao);
	RemoveValueFromList(t->professores, professor);
	PrintLine("Professor removido");
}

void AdicionarDisciplinaTurma(Cadastros* c, Turma* t) {
	ListarDisciplinas(c->disciplinasCadastradas);
	int codigo = ReadInt("\nCodigo da disciplina: ");
	Disciplina* d = BuscarDisciplinaCodigo(c->disciplinasCadastradas, codigo);

	if (BuscarDisciplinaCodigo(t->disciplinas, codigo) != NULL) {
		PrintLine("Disciplina ja adicionada");
		return;
	}

	if (d == NULL) {
		PrintLine("Disciplina nao encontrada");
		return;
	}

	d = Clone(d, sizeof(Disciplina));
	d->nome = CloneString(d->nome);
	d->preRequisitos = CloneList(d->preRequisitos, sizeof(int));

	AddValueToList(t->disciplinas, d);
}

void RemoverDisciplinaTurma(Cadastros* c, Turma* t) {
	ListarDisciplinas(c->disciplinasCadastradas);
	int codigo = ReadInt("\nCodigo da disciplina: ");
	Disciplina* d = BuscarDisciplinaCodigo(t->disciplinas, codigo);

	if (d == NULL) {
		PrintLine("Disciplina nao cadastrada na turma");
		return;
	}

	free(d->nome);
	FreeList(d->preRequisitos);

	RemoveValueFromList(t->disciplinas, d);
	PrintLine("Disciplina removida");
}