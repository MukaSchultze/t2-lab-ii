#pragma once
#include "cadastros.h"
#include "list.h"

typedef struct {
	int siape;
	char* nome;
	char* area;
	char* titulacao;
} Professor;

void CadastrarProfessor(Cadastros* c);
void RemoverProfessor(Cadastros* c);
void BuscarProfessor(Cadastros* c);
void PrintProfessor(Professor p);

Professor* BuscarProfessorSiape(List* l, int siape);