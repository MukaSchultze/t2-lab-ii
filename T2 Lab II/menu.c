#include <stdlib.h> 
#include "menu.h"
#include "io.h"
#include "aluno.h"
#include "professor.h"
#include "disciplina.h"
#include "list.h"
#include "turma.h"
#include "avaliacao.h"
#include "aula.h"
#include "curso.h"
#include "relatorio.h"

void DoMenu(Cadastros* cadastros) {

	int opt;

	do {
		PrintLine("");
		PrintLine("MENU");
		PrintLine("0 - Sair");
		PrintLine("1 - Cadastrar");
		PrintLine("2 - Remover");
		PrintLine("3 - Buscar");
		PrintLine("4 - Turmas");
		PrintLine("5 - Lancar aula");
		PrintLine("");

		opt = ReadInt("Digite a opcao: ");
		ClearConsole();

		switch (opt) {
			case 0: return;
			case 1: CadastroMenu(cadastros); break;
			case 2: RemoveMenu(cadastros); break;
			case 3: BuscaMenu(cadastros); break;
			case 4: TurmasMenu(cadastros); break;
			case 5: LancarAula(cadastros); break;
			default: PrintLine("Opcao Invalida"); break;
		}
	} while (opt);

}

void CadastroMenu(Cadastros* cadastros) {

	int opt;

	do {
		PrintLine("");
		PrintLine("CADASTRAR");
		PrintLine("0 - Voltar");
		PrintLine("1 - Aluno");
		PrintLine("2 - Professor");
		PrintLine("3 - Disciplina");
		PrintLine("4 - Turma");
		PrintLine("5 - Avaliacao");
		PrintLine("6 - Cursos");
		PrintLine("");

		opt = ReadInt("Digite a opcao: ");
		ClearConsole();

		switch (opt) {
			case 0: return;
			case 1: CadastrarAluno(cadastros); return;
			case 2: CadastrarProfessor(cadastros); return;
			case 3: CadastrarDisciplina(cadastros); return;
			case 4: CadastrarTurma(cadastros); return;
			case 5: LancarAvaliacao(cadastros); return;
			case 6: CadastrarCurso(cadastros); return;
			default: PrintLine("Opcao Invalida"); break;
		}
	} while (opt);

}

void RemoveMenu(Cadastros* cadastros) {

	int opt;

	do {
		PrintLine("");
		PrintLine("REMOVER");
		PrintLine("0 - Voltar");
		PrintLine("1 - Aluno");
		PrintLine("2 - Professor");
		PrintLine("3 - Disciplina");
		PrintLine("4 - Turma");
		PrintLine("5 - Avaliacao");
		PrintLine("");

		opt = ReadInt("Digite a opcao: ");
		ClearConsole();

		switch (opt) {
			case 0: return;
			case 1: RemoverAluno(cadastros); return;
			case 2: RemoverProfessor(cadastros); return;
			case 3: RemoverDisciplina(cadastros); return;
			case 4: RemoverTurma(cadastros); return;
			case 5: RemoverAvaliacao(cadastros); return;
			default: PrintLine("Opcao Invalida"); break;
		}
	} while (opt);

}

void BuscaMenu(Cadastros* cadastros) {

	int opt;

	do {
		PrintLine("");
		PrintLine("BUSCAR");
		PrintLine("0 - Voltar");
		PrintLine("1 - Aluno");
		PrintLine("2 - Professor");
		PrintLine("3 - Disciplina");
		PrintLine("4 - Turma");
		PrintLine("5 - Avaliacao");
		PrintLine("");

		opt = ReadInt("Digite a opcao: ");
		ClearConsole();

		switch (opt) {
			case 0: return;
			case 1: BuscarAluno(cadastros); return;
			case 2: BuscarProfessor(cadastros); return;
			case 3: BuscarDisciplina(cadastros); return;
			case 4: BuscarTurma(cadastros); return;
			case 5: BuscarAvaliacao(cadastros); return;
			default: PrintLine("Opcao Invalida"); break;
		}
	} while (opt);

}

void TurmasMenu(Cadastros* cadastros) {
	int opt;
	Turma* turma = NULL;

	do {
		PrintLine("");
		PrintLine("TURMAS");
		PrintLine("0 - Voltar");
		PrintLine("1 - Matricular Aluno");
		PrintLine("2 - Remover Aluno");
		PrintLine("3 - Adicionar Professor");
		PrintLine("4 - Remover Professor");
		PrintLine("5 - Adicionar Disciplina");
		PrintLine("6 - Remover Disciplina");
		PrintLine("7 - Gerar Relatorio");
		PrintLine("");

		opt = ReadInt("Digite a opcao: ");

		if (opt) {
			ListarTurmas(cadastros->turmasCadastradas);
			int codigoTurma = ReadInt("\nDigite o codigo da turma: ");
			turma = BuscarTurmaCodigo(cadastros->turmasCadastradas, codigoTurma);

			if (turma == NULL) {
				PrintLine("Turma nao encontrada");
				continue;
			}
		}

		switch (opt) {
			case 0: return;
			case 1: MatricularAlunoTurma(cadastros, turma); return;
			case 2: RemoverAlunoTurma(cadastros, turma); return;
			case 3: AdicionarProfessorTurma(cadastros, turma); return;
			case 4: RemoverProfessorTurma(cadastros, turma); return;
			case 5: AdicionarDisciplinaTurma(cadastros, turma); return;
			case 6: RemoverDisciplinaTurma(cadastros, turma); return;
			case 7: GerarRelatorio(cadastros, turma); return;
			default: PrintLine("Opcao Invalida"); break;
		}
	} while (opt);

}