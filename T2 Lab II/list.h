#pragma once
#include <stdbool.h>

typedef struct listNode {
	void* value;
	struct listNode* next;
	struct listNode* prev;
} ListNode;

typedef struct list {
	int length;
	ListNode* firstElement;
	ListNode* lastElement;
} List;

List* CreateNewList();
void FreeList(List* list);

ListNode* CreateNewNode(void* value);
ListNode* SearchValue(List* list, void* value);

void AddValueToList(List* list, void* value);
bool RemoveValueFromList(List* list, void* value);

void AddNodeToList(List* list, ListNode* node);
bool RemoveNodeFromList(List* list, ListNode* node);
