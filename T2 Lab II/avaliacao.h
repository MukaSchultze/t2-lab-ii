#pragma once
#include "cadastros.h"
#include "list.h"
#include "turma.h"
#include "curso.h"

typedef struct {
	int codigoDisciplina;
	int codigo;
	List* notas;
} Avaliacao;

typedef struct {
	int matriculaAluno;
	float nota;
} Nota;

void LancarAvaliacao(Cadastros* cadastros);
void RemoverAvaliacao(Cadastros* cadastros);
void BuscarAvaliacao(Cadastros* cadastros);

Avaliacao* BuscarAvaliacaoCodigo(Turma* t, int codigo);