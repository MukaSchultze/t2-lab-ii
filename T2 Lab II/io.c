#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "io.h"
#include "aluno.h"
#include "professor.h"
#include "disciplina.h"
#include "turma.h"
#include "avaliacao.h"
#include "curso.h"

void* Clone(void* src, int length) {
	char* dst = malloc(length);

	for (int i = 0; i < length; i++)
		dst[i] = ((char*)src)[i];

	return dst;
}

char* CloneString(char* src) {
	return Clone(src, strlen(src) + 1);
}

List* CloneList(List* l, int valueSize) {
	List* newL = CreateNewList();

	for (ListNode* node = l->firstElement; node != NULL; node = node->next)
		AddValueToList(newL, Clone(node->value, valueSize));

	return newL;
}

void ClearConsole() {
	printf("\n\n\n");
}

void PrintLine(char* str) {
	printf("%s\n", str);
}

int ReadInt(char* label) {
	int result;
	printf("%s", label);
	scanf("%d", &result);
	return result;
}

float ReadFloat(char* label) {
	float result;
	printf("%s", label);
	scanf("%f", &result);
	return result;
}

char* ReadString(char* label) {
	char str[1024];
	printf("%s", label);

	str[0] = getchar(); //Perdi uma hora e meia pra fazer o fgets funcionar...

	if (str[0] == '\n' || str[0] == '\0')
		fgets(str, sizeof(str), stdin);
	else
		fgets(str + 1, sizeof(str) - 1, stdin);

	str[strlen(str) - 1] = '\0';

	return CloneString(str);
}

void ListarAlunos(List* l) {
	PrintLine("Alunos disponiveis");
	for (ListNode* n = l->firstElement; n != NULL; n = n->next) {
		Aluno* v = n->value;
		printf("\t%d - %s\n", v->matricula, v->nome);
	}
}

void ListarProfessores(List* l) {
	PrintLine("Professores disponiveis");
	for (ListNode* n = l->firstElement; n != NULL; n = n->next) {
		Professor* v = n->value;
		printf("\t%d - %s\n", v->siape, v->nome);
	}
}

void ListarAvaliacoes(List* l) {
	PrintLine("Avaliacoes disponiveis");
	for (ListNode* n = l->firstElement; n != NULL; n = n->next) {
		Avaliacao* v = n->value;
		printf("\tAvaliacao %d\n", v->codigo);
	}
}

void ListarCursos(List* l) {
	PrintLine("Cursos disponiveis");
	for (ListNode* n = l->firstElement; n != NULL; n = n->next) {
		Curso* v = n->value;
		printf("\t%d - %s\n", v->codigo, v->nome);
	}
}

void ListarDisciplinas(List* l) {
	PrintLine("Disciplinas disponiveis");
	for (ListNode* n = l->firstElement; n != NULL; n = n->next) {
		Disciplina* v = n->value;
		printf("\t%d - %s\n", v->codigo, v->nome);
	}
}

void ListarTurmas(List* l) {
	PrintLine("Turmas disponiveis");
	for (ListNode* n = l->firstElement; n != NULL; n = n->next) {
		Turma* v = n->value;
		printf("\t%d \n", v->codigo);
	}
}