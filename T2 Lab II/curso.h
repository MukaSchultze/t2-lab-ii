#pragma once
#include "list.h"
#include "cadastros.h"

typedef struct {
	int codigo;
	char* nome;
	//List* avaliacoes;  
} Curso;

void CadastrarCurso(Cadastros* c);

Curso* BuscaCursoCodigo(List* l, int codigo);