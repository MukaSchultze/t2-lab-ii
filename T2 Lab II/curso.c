#include <stdlib.h>
#include "curso.h"
#include "io.h"

void CadastrarCurso(Cadastros* c) {
	Curso* curso = malloc(sizeof(Curso));

	curso->nome = ReadString("Nome do curso: ");
	curso->codigo = 0;

	while (!curso->codigo)
		curso->codigo = ReadInt("Codigo do curso: ");

	AddValueToList(c->cursos, curso);
}

Curso* BuscaCursoCodigo(List* l, int codigo) {
	for (ListNode* node = l->firstElement; node != NULL; node = node->next) {
		Curso* c = node->value;

		if (c->codigo == codigo)
			return c;
	}

	return NULL;
}