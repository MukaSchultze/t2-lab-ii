#include <stdlib.h>
#include <stdio.h>
#include "aluno.h"
#include "io.h"
#include "list.h"

void CadastrarAluno(Cadastros* c) {
	Aluno* a = malloc(sizeof(Aluno));

	PrintLine("Informacoes do aluno");

	a->nome = ReadString("Nome: ");
	a->matricula = 0;
	a->cursoCodigo = 0;
	ListarCursos(c->cursos);

	while (BuscaCursoCodigo(c->cursos, a->cursoCodigo) == NULL)
		a->cursoCodigo = ReadInt("\nCodigo do curso: ");

	while (a->matricula == 0)
		a->matricula = ReadInt("Matricula: ");

	AddValueToList(c->alunosCadastrados, a);
}

void RemoverAluno(Cadastros* c) {
	PrintLine("Remover aluno\n");
	ListarAlunos(c->alunosCadastrados);
	int matricula = ReadInt("\nMatricula (0 para voltar): ");

	if (matricula == 0)
		return;

	for (ListNode* node = c->alunosCadastrados->firstElement; node != NULL; node = node->next) {
		Aluno* a = node->value;

		if (a->matricula == matricula) {
			printf("%s (%d) removido\n", a->nome, a->matricula);
			free(a->nome);
			RemoveNodeFromList(c->alunosCadastrados, node);
			return;
		}
	}

	PrintLine("Matricula nao encontrada");
}

void BuscarAluno(Cadastros* c) {
	PrintLine("Buscar aluno\n");
	ListarAlunos(c->alunosCadastrados);
	int matricula = ReadInt("\nMatricula (0 para voltar): ");
	PrintLine("");

	if (matricula == 0)
		return;

	Aluno* a = BuscarAlunoMatricula(c->alunosCadastrados, matricula);

	if (a != NULL)
		PrintAluno(c, *a);
	else
		PrintLine("Matricula nao encontrada");
}

void PrintAluno(Cadastros* c, Aluno a) {

	Curso* curso = BuscaCursoCodigo(c->cursos, a.cursoCodigo);
	char* nomeCurso;

	if (curso == NULL)
		nomeCurso = "Invalido";
	else
		nomeCurso = curso->nome;

	printf("Nome: %s\nMatricula: %d\nCurso: %s\n", a.nome, a.matricula, nomeCurso);
}

Aluno* BuscarAlunoMatricula(List* l, int matricula) {
	for (ListNode* node = l->firstElement; node != NULL; node = node->next) {
		Aluno* a = node->value;

		if (a->matricula == matricula)
			return a;
	}

	return NULL;
}