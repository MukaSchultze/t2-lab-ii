#pragma once
#include "cadastros.h"
#include "list.h"

typedef struct {
	int codigo;
	List* professores;
	List* alunos;
	List* disciplinas;
	List* aulas;
	List* avaliacoes;
} Turma;

void CadastrarTurma(Cadastros* c);
void RemoverTurma(Cadastros* c);
void BuscarTurma(Cadastros* c);
void PrintTurma(Turma t);

Turma* BuscarTurmaCodigo(List* l, int codigoTurma);

void MatricularAlunoTurma(Cadastros* c, Turma* t);
void RemoverAlunoTurma(Cadastros* c, Turma* t);

void AdicionarProfessorTurma(Cadastros* c, Turma* t);
void RemoverProfessorTurma(Cadastros* c, Turma* t);

void AdicionarDisciplinaTurma(Cadastros* c, Turma* t);
void RemoverDisciplinaTurma(Cadastros* c, Turma* t);