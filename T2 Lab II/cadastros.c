#include <stdlib.h>
#include "cadastros.h"

Cadastros* CriarCadastros() {
	Cadastros* r = malloc(sizeof(Cadastros));

	r->alunosCadastrados = CreateNewList();
	//r->avaliacoesCadastradas = CreateNewList();
	r->disciplinasCadastradas = CreateNewList();
	r->professoresCadastrados = CreateNewList();
	r->turmasCadastradas = CreateNewList();
	r->cursos = CreateNewList();

	return r;
}