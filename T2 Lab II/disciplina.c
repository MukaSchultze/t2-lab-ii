#include <stdlib.h>
#include <stdio.h>
#include "disciplina.h"
#include "io.h"
#include "list.h"

void CadastrarDisciplina(Cadastros* c) {
	Disciplina* d = malloc(sizeof(Disciplina));

	PrintLine("Informacoes do Disciplina");

	d->nome = ReadString("Nome: ");

	ListarCursos(c->cursos);
	d->curso = BuscaCursoCodigo(c->cursos, ReadInt("\nCodigo do curso: "));

	if (d->curso == NULL) {
		free(d->nome);
		free(d);
		PrintLine("Curso nao encontrado");
		return;
	}

	d->cargaHoraria = ReadInt("Carga horaria: ");
	d->preRequisitos = CreateNewList();
	d->codigo = 0;

	while (d->codigo == 0)
		d->codigo = ReadInt("Codigo: ");

	int pr;

	ListarDisciplinas(c->disciplinasCadastradas);
	PrintLine("");

	do {
		pr = ReadInt("Codigo do pre requisito (0 para cancelar): ");

		if (pr)
			AddValueToList(d->preRequisitos, Clone(&pr, sizeof(pr)));
	} while (pr);

	AddValueToList(c->disciplinasCadastradas, d);
}

void RemoverDisciplina(Cadastros* c) {
	PrintLine("\nRemover Disciplina");
	ListarDisciplinas(c->disciplinasCadastradas);
	int codigo = ReadInt("\nCodigo (0 para voltar): ");

	if (codigo == 0)
		return;

	for (ListNode* node = c->disciplinasCadastradas->firstElement; node != NULL; node = node->next) {
		Disciplina* d = node->value;

		if (d->codigo == codigo) {
			printf("%s (%d) removida\n", d->nome, d->codigo);
			free(d->nome);
			FreeList(d->preRequisitos);
			RemoveNodeFromList(c->disciplinasCadastradas, node);
			return;
		}
	}

	PrintLine("Codigo nao encontrado");
}

void BuscarDisciplina(Cadastros* c) {
	PrintLine("Buscar Disciplina\n");
	ListarDisciplinas(c->disciplinasCadastradas);
	int codigo = ReadInt("\nCodigo (0 para voltar): ");
	PrintLine("");

	if (codigo == 0)
		return;

	for (ListNode* node = c->disciplinasCadastradas->firstElement; node != NULL; node = node->next) {
		Disciplina* d = node->value;

		if (d->codigo == codigo) {
			PrintDisciplina(c, *d);
			return;
		}
	}

	PrintLine("Codigo nao encontrado");
}

void PrintDisciplina(Cadastros* c, Disciplina d) {
	printf("Nome: %s\nCodigo: %d\nCarga horaria: %d\nPre-requisitos\nCurso: %s\n", d.nome, d.codigo, d.cargaHoraria, d.curso->nome);

	for (ListNode* node = d.preRequisitos->firstElement; node != NULL; node = node->next) {
		int* codigo = node->value;
		Disciplina* d = BuscarDisciplinaCodigo(c->disciplinasCadastradas, *codigo);

		if (d != NULL)
			printf("\t%s (%d)\n", d->nome, d->codigo);
	}
}

Disciplina* BuscarDisciplinaCodigo(List* l, int codigoDisc) {
	for (ListNode* node = l->firstElement; node != NULL; node = node->next) {
		Disciplina* d = node->value;

		if (d->codigo == codigoDisc)
			return d;
	}

	return NULL;
}