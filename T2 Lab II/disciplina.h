#pragma once
#include "cadastros.h"
#include "list.h"
#include "curso.h"

typedef struct {
	int codigo;
	char* nome;
	int cargaHoraria;
	Curso* curso;
	List* preRequisitos; //Codigo das disciplinas (int)
} Disciplina;

void CadastrarDisciplina(Cadastros* c);
void RemoverDisciplina(Cadastros* c);
void BuscarDisciplina(Cadastros* c);
void PrintDisciplina(Cadastros* c, Disciplina d);

Disciplina* BuscarDisciplinaCodigo(List* l, int codigoDisc);