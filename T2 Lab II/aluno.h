#pragma once
#include "cadastros.h"
#include "curso.h"

typedef struct {
	int matricula;
	char* nome;
	int cursoCodigo;
} Aluno;

void CadastrarAluno(Cadastros* c);
void RemoverAluno(Cadastros* c);
void BuscarAluno(Cadastros* c);
void PrintAluno(Cadastros* c, Aluno a);

Aluno* BuscarAlunoMatricula(List* l, int matricula);