#pragma once
#include "cadastros.h"
#include "turma.h"

typedef struct {
	int matriculaAluno;
	int aulasTotais;
	int aulasPresente;
	float notaAvaliacoes;
} Relatorio;

void PrintRelatorio(Cadastros* c, Turma* t, Relatorio* r);
Relatorio* BuscarRelatorioAluno(List* l, int matricula);
void GerarRelatorio(Cadastros* cadastros, Turma* t);