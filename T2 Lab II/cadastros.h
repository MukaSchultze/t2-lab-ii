#pragma once
#include "list.h"

typedef struct {

	List* alunosCadastrados;
	List* professoresCadastrados;
	List* disciplinasCadastradas;
	List* turmasCadastradas;
	List* cursos;

} Cadastros;

Cadastros* CriarCadastros();