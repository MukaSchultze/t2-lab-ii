#include <stdio.h>
#include <stdlib.h>
#include "relatorio.h"
#include "menu.h"
#include "io.h"
#include "aluno.h"
#include "professor.h"
#include "disciplina.h"
#include "list.h"
#include "turma.h"
#include "avaliacao.h"
#include "aula.h"
#include "curso.h"

void PrintRelatorio(Cadastros* c, Turma* t, Relatorio* r) {
	Aluno* a = BuscarAlunoMatricula(t->alunos, r->matriculaAluno);

	if (a == NULL) {
		printf("Aluno nao encontrado\n");
		return;
	}

	if (r->aulasTotais <= 0)
		r->aulasTotais = 1;

	float nota = r->notaAvaliacoes;
	float freq = (float)r->aulasPresente / r->aulasTotais;
	char* status;

	if (freq < 0.75)
		status = "Reprovado por frequencia";
	else if (nota < 7)
		status = "Reprovado por nota";
	else
		status = "Aprovado";

	printf("%s (%d): Nota - %.2f\tFrequencia - %.0f%%\t - %s\n", a->nome, a->matricula, nota, freq * 100, status);
}

Relatorio* BuscarRelatorioAluno(List* l, int matricula) {
	for (ListNode* n = l->firstElement; n != NULL; n = n->next) {
		Relatorio* r = n->value;

		if (r->matriculaAluno == matricula)
			return r;
	}

	Relatorio* r = malloc(sizeof(Relatorio));

	r->matriculaAluno = matricula;
	r->aulasPresente = 0;
	r->aulasTotais = 0;
	r->notaAvaliacoes = 0;
	AddValueToList(l, r);

	return r;
}

void GerarRelatorio(Cadastros* cadastros, Turma* t) {
	List* relatorios = CreateNewList();

	for (ListNode* n1 = t->avaliacoes->firstElement; n1 != NULL; n1 = n1->next) {
		Avaliacao* a = n1->value;

		for (ListNode* n2 = a->notas->firstElement; n2 != NULL; n2 = n2->next) {
			Nota* n = n2->value;
			Relatorio* r = BuscarRelatorioAluno(relatorios, n->matriculaAluno);
			r->notaAvaliacoes += (float)n->nota / t->avaliacoes->length;
		}
	}

	for (ListNode* n1 = t->aulas->firstElement; n1 != NULL; n1 = n1->next) {
		Aula* a = n1->value;

		for (ListNode* n2 = a->matriculaAlunosPresentes->firstElement; n2 != NULL; n2 = n2->next) {
			int* matricula = n2->value;
			Relatorio* r = BuscarRelatorioAluno(relatorios, *matricula);
			r->aulasPresente++;
			r->aulasTotais = t->aulas->length;
		}
	}

	for (ListNode* n = relatorios->firstElement; n != NULL; n = n->next)
		PrintRelatorio(cadastros, t, n->value);

	FreeList(relatorios);
}