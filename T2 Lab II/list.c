#include <stdlib.h>
#include "list.h"

List* CreateNewList() {
	List* result = malloc(sizeof(List));

	result->firstElement = NULL;
	result->lastElement = NULL;
	result->length = 0;

	return result;
}

void FreeList(List* list) {
	while (list->firstElement != NULL)
		RemoveNodeFromList(list, list->firstElement);

	free(list);
}

ListNode* CreateNewNode(void* value) {
	ListNode* node = malloc(sizeof(ListNode));

	node->value = value;
	node->next = NULL;
	node->prev = NULL;

	return node;
}

ListNode* SearchValue(List* list, void* value) {
	for (ListNode* node = list->firstElement; node != NULL; node = node->next)
		if (node->value == value)
			return node;

	return NULL;
}

void AddValueToList(List* list, void* value) {
	AddNodeToList(list, CreateNewNode(value));
}

bool RemoveValueFromList(List* list, void* value) {
	return RemoveNodeFromList(list, SearchValue(list, value));
}

void AddNodeToList(List* list, ListNode* node) {
	if (list->firstElement == NULL)
		list->firstElement = node;
	else {
		node->prev = list->lastElement;
		list->lastElement->next = node;
	}

	list->lastElement = node;
	list->length++;
}

bool RemoveNodeFromList(List* list, ListNode* node) {
	ListNode* next = node->next;
	ListNode* prev = node->prev;

	for (ListNode* i = list->firstElement; i != NULL; i = i->next)
		if (i == node) {
			if (i->prev != NULL)
				i->prev->next = next;
			if (i->next != NULL)
				i->next->prev = prev;

			if (list->firstElement == node)
				list->firstElement = next;
			if (list->lastElement)
				list->lastElement = prev;

			free(node->value);
			free(node);

			list->length--;
			return true;
		}

	return false;
}