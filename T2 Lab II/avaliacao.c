#include <stdlib.h>
#include <stdio.h>
#include "avaliacao.h"
#include "io.h"
#include "disciplina.h"
#include "aluno.h"
#include "turma.h"

void LancarAvaliacao(Cadastros* c) {
	ListarTurmas(c->turmasCadastradas);
	Turma* t = BuscarTurmaCodigo(c->turmasCadastradas, ReadInt("Codigo da turma: "));

	if (t == NULL) {
		PrintLine("Turma nao encontrada");
		return;
	}

	ListarDisciplinas(t->disciplinas);
	Disciplina* d = BuscarDisciplinaCodigo(c->disciplinasCadastradas, ReadInt("Codigo da disciplina: "));

	if (d == NULL) {
		PrintLine("Disciplina nao encontrada");
		return;
	}

	Avaliacao* a = malloc(sizeof(Avaliacao));

	a->notas = CreateNewList();
	a->codigoDisciplina = d->codigo;
	a->codigo = 0;

	while (a->codigo == 0)
		a->codigo = ReadInt("Codigo da avaliacao: ");

	ListarAlunos(c->alunosCadastrados);
	PrintLine("");

	while (true) {
		Nota nota;
		nota.matriculaAluno = ReadInt("Matricula do aluno (0 para cancelar): ");

		if (nota.matriculaAluno == 0)
			break;

		nota.nota = ReadFloat("Nota: ");
		AddValueToList(a->notas, Clone(&nota, sizeof(Nota)));
	}

	AddValueToList(t->avaliacoes, a);
}

void RemoverAvaliacao(Cadastros* c) {
	ListarTurmas(c->turmasCadastradas);
	Turma* t = BuscarTurmaCodigo(c->turmasCadastradas, ReadInt("Codigo da turma: "));

	if (t == NULL) {
		PrintLine("Turma nao encontrada");
		return;
	}

	ListarDisciplinas(t->disciplinas);
	Disciplina* d = BuscarDisciplinaCodigo(c->disciplinasCadastradas, ReadInt("Codigo da disciplina: "));

	if (d == NULL) {
		PrintLine("Disciplina nao encontrada");
		return;
	}

	ListarAvaliacoes(t->avaliacoes);
	Avaliacao* a = BuscarAvaliacaoCodigo(t, ReadInt("\nCodigo da avaliacao"));

	if (a == NULL) {
		PrintLine("Avaliacao nao encontrada");
		return;
	}

	FreeList(a->notas);
	RemoveValueFromList(t->avaliacoes, a);
	PrintLine("Avaliacao removida");
}

void BuscarAvaliacao(Cadastros* cadastros) {
	ListarTurmas(cadastros->turmasCadastradas);
	Turma* t = BuscarTurmaCodigo(cadastros->turmasCadastradas, ReadInt("Codigo da turma: "));

	if (t == NULL) {
		PrintLine("Turma nao encontrada");
		return;
	}

	ListarDisciplinas(t->disciplinas);
	Disciplina* d = BuscarDisciplinaCodigo(cadastros->disciplinasCadastradas, ReadInt("Codigo da disciplina: "));

	if (d == NULL) {
		PrintLine("Disciplina nao encontrada");
		return;
	}

	ListarAvaliacoes(t->avaliacoes);
	Avaliacao* a = BuscarAvaliacaoCodigo(t, ReadInt("\nCodigo da avaliacao"));

	if (a == NULL) {
		PrintLine("Avaliacao nao encontrada");
		return;
	}

	printf("Avaliacao: %d\n", a->codigo);

	for (ListNode* n = a->notas->firstElement; n != NULL; n = n->next) {
		Nota* nota = n->value;
		Aluno* aluno = BuscarAlunoMatricula(cadastros->alunosCadastrados, nota->matriculaAluno);

		if (aluno == NULL)
			printf("Aluno %d nao encontrado\n", nota->matriculaAluno);
		else
			printf("%s(%d) - %f\n", aluno->nome, nota->matriculaAluno, nota->nota);
	}
}

Avaliacao* BuscarAvaliacaoCodigo(Turma* t, int codigo) {
	for (ListNode* n = t->avaliacoes->firstElement; n != NULL; n = n->next) {
		Avaliacao* a = n->value;

		if (a->codigo == codigo)
			return a;
	}

	return NULL;
}