#pragma once
#include "list.h"

void* Clone(void* src, int length);
char* CloneString(char* src);
List* CloneList(List* l, int valueSize);
void ClearConsole();
void PrintLine(char* str);
int ReadInt(char* label);
float ReadFloat(char* label);
char* ReadString(char* label);

void ListarAlunos(List* l);
void ListarProfessores(List* l);
void ListarAvaliacoes(List* l);
void ListarCursos(List* l);
void ListarDisciplinas(List* l);
void ListarTurmas(List* l);