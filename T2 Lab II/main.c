#include <stdio.h>
#include <stdlib.h>
#include "menu.h"
#include "cadastros.h"
#include "io.h"

/*
* Samuel Schultze - SI - 09/04/2018 - Lab II
* gcc -Wall *.c
*/

int main() {
	ClearConsole();
	Cadastros* cadastros = CriarCadastros();
	DoMenu(cadastros);
	free(cadastros);
}